# ![logo](client/public/images/logo.png) React Boilerplate

![react](https://img.shields.io/badge/react-16.6.3-blue.svg)
![babel](https://img.shields.io/badge/babel-7.2.0-orange.svg)
![license](https://img.shields.io/badge/license-CC_BY--NC--SA-green.svg)

![working](https://img.shields.io/badge/⚠_in_working_⚠-red.svg)

The boilerplate is not finished. Please waiting the moment ...

## Install

It's really simple:

> $ npm i

You can run this command to begin:

> $ npm start

## Includes

### 1. Client

#### Behaviours

- 🔥 loader + 🔥 middleware
- Server Side Rendering

#### Managers

- Redux
- Lang
- Routes

#### Components

- Material UI
- FontAwesome

### 2. Server

#### Managers

- Sessions
- Socket.IO

#### Connectors

- MongoDb
- Firebase

## Authors

- Yarflam - *initial work*

## License

The project is licensed under Creative Commons (BY-NC-SA) - see the [LICENSE.md](LICENSE.md) file for details.
