module.exports = {
    parser: require('postcss-comment'),
    plugins: {
        'postcss-import': {},
        'postcss-flexbugs-fixes': {},
        'postcss-extend-rule': {},
        'postcss-advanced-variables': {},
        'postcss-simple-vars': {},
        'postcss-utilities': {},
        'postcss-preset-env': {
            stage: 0,
            features: {
                'nesting-rules': true,
                'color-mod-function': true,
                'custom-media': true
            }
        }, // nesting + applying polyfills
        'postcss-sorting': {},
        autoprefixer: {}
    }
};
