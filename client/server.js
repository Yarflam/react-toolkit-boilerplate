import express from 'express';
import http from 'http';
import path from 'path';
import fs from 'fs';

import webpack from 'webpack';
import webpackConfig from '../config/webpack.dev';

import configPublic from '../config/public.cfg.json';

/* Define the server */
const app = express();
const server = http.Server(app);

/* Dev middleware */
var compiler = webpack(webpackConfig);
app.use(
    require('webpack-dev-middleware')(compiler, { ...webpackConfig.devServer })
);
app.use(
    require('webpack-hot-middleware')(compiler, { ...webpackConfig.devServer })
);

/* Expose */
app.use(
    '/',
    express.static(path.resolve(__dirname, ...configPublic.client.expose))
);

/* Every requests on the website */
app.get(/\/?.+/, (req, res) => {
    res.header('Status', 200);
    res.writeHead(200, { 'Content-Type': 'text/html; charset=utf-8' });
    res.write(
        fs.readFileSync(
            path.resolve(
                __dirname,
                '..',
                ...configPublic.client.expose,
                'index.html'
            )
        )
    );
    res.end();
});

/* Listener */
server.listen(configPublic.client.port, '0.0.0.0', () => {
    console.log('The server is started on port ' + configPublic.client.port);
});
