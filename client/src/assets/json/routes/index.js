import any from './routes.any.json';
import fr from './routes.fr.json';
import en from './routes.en.json';

function applySuffix(routes, lang) {
    return (routes || []).map(route => ({ ...route, lang }));
}

export default [...any, ...applySuffix(fr, 'fr'), ...applySuffix(en, 'en')];
