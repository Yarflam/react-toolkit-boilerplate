import React, { Fragment } from 'react';
import { NavLink } from 'react-router-dom';

import { lang } from 'root/classes';

import './footer.pcss';

function Footer() {
    return (
        <nav>
            <NavLink exact to="/" activeClassName="selected">
                {lang.print('footer.index')}
            </NavLink>
            <NavLink exact to="/apropos" activeClassName="selected">
                {'A propos'}
            </NavLink>
            <NavLink exact to="/about" activeClassName="selected">
                {'About'}
            </NavLink>
        </nav>
    );
}

export default Footer;
