import React from 'react';
import PropTypes from 'prop-types';
import { AppBar, Toolbar, Button, Typography, Grid } from '@material-ui/core';

import { store } from 'store';

function Header(props) {
    const setLang = lang => () => {
        props.setLang(lang);
    };

    return (
        <AppBar position="static">
            <Toolbar>
                <Grid container>
                    <Grid item xs={6}>
                        <Typography variant="h6" color="inherit">
                            React Boilerplate
                        </Typography>
                    </Grid>
                    <Grid item xs={6} className="text-right">
                        <Button onClick={setLang('fr')} color="inherit">
                            Français
                        </Button>
                        <Button onClick={setLang('en')} color="inherit">
                            English
                        </Button>
                    </Grid>
                </Grid>
            </Toolbar>
        </AppBar>
    );
}

Header.propTypes = {
    setLang: PropTypes.func.isRequired
};

export default store.connect([], ['setLang'])(Header);
