import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import './basic.pcss';

function Basic(props) {
    return (
        <Fragment>
            <header>
                <div>{props.header}</div>
            </header>
            <section>{props.section}</section>
            <footer>{props.footer}</footer>
        </Fragment>
    );
}

Basic.propTypes = {
    header: PropTypes.any.isRequired,
    section: PropTypes.any.isRequired,
    footer: PropTypes.any.isRequired
};

export default Basic;
