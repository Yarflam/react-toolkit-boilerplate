import React from 'react';
import PropTypes from 'prop-types';
import CarpetCell from './carpet';
import styles from './sierpinski.pcss';

function Carpet(props) {
    let n = props.n || 0;
    return n ? (
        <div className={styles.sierpContainer}>
            {new Array(9)
                .fill(0)
                .map((_, index) =>
                    index != 4 ? (
                        <CarpetCell key={`sierp:${n}:${index}`} n={n - 1} />
                    ) : (
                        <div
                            key={`sierp:${n}:${index}`}
                            className={styles.sierpSqWhite}
                        />
                    )
                )}
        </div>
    ) : (
        <div className={styles.sierpSqBlack} />
    );
}

Carpet.propTypes = {
    n: PropTypes.number
};

export default Carpet;
