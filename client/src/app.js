import React, { Component } from 'react';
import { hot } from 'react-hot-loader/root';
import { StoreProvider, LangProvider, RoutesProvider, store } from './classes';
import './assets/styles/global.pcss';

class App extends Component {
    render() {
        return (
            <StoreProvider>
                <LangProvider lang="auto" defaultLang="en">
                    <RoutesProvider notFound="pages/error404/" />
                </LangProvider>
            </StoreProvider>
        );
    }
}

export default hot(App);
