import React, { Component, Fragment } from 'react';

import { Basic, Header, Footer } from 'root/components';
import { lang, FaIcon } from 'root/classes';

class HomePage extends Component {
    render() {
        return (
            <Basic
                header={<Header />}
                section={
                    <Fragment>
                        <p>{lang.print('page.home', <FaIcon home />)}</p>
                    </Fragment>
                }
                footer={<Footer />}
            />
        );
    }
}

export default HomePage;
