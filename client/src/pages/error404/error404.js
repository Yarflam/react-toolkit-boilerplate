import React, { Component } from 'react';

import { Basic, Header, Footer } from 'root/components';
import { lang } from 'root/classes';

class Error404 extends Component {
    render() {
        return (
            <Basic
                header={<Header />}
                section={<p>{lang.print('site.error404')}</p>}
                footer={<Footer />}
            />
        );
    }
}

export default Error404;
