import React, { Component, Fragment } from 'react';

import { Basic, Header, Footer, SierpinskiCarpet } from 'root/components';
import { lang } from 'root/classes';

class About extends Component {
    render() {
        return (
            <Basic
                header={<Header />}
                section={
                    <Fragment>
                        <p>{lang.print('page.about.text')}</p>
                        <div className="text-center">
                            <div
                                style={{
                                    display: 'inline-block',
                                    width: '200px',
                                    height: '200px'
                                }}
                                className="mb-1"
                            >
                                <SierpinskiCarpet n={3} />
                            </div>
                            <p>
                                <i>{lang.print('page.about.carpet')}</i>
                            </p>
                        </div>
                    </Fragment>
                }
                footer={<Footer />}
            />
        );
    }
}

export default About;
