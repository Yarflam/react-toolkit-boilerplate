import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';

class ScrollTop extends Component {
    componentDidUpdate(props) {
        if (this.props.location != props.location) {
            window.scrollTo(0, 0);
        }
    }

    render() {
        return this.props.children;
    }
}

ScrollTop.propTypes = {
    location: PropTypes.object,
    children: PropTypes.any
};

export default withRouter(ScrollTop);
