import React, { Fragment } from 'react';
import uniqid from 'uniqid';

const tools = {
    replaceHTML: (input, search, replace, all = false) => {
        /* React support */
        if (React.isValidElement(replace)) {
            replace = <Fragment key={uniqid()}>{replace}</Fragment>;
        }
        /* Replacement */
        if (typeof input == 'string') {
            let shards = input.split(search);
            return shards.length > 1
                ? !all
                    ? [
                        shards[0],
                        replace,
                        input.substr(shards[0].length + search.length)
                    ]
                    : shards
                        .reduce(
                            (accum, shard) => accum.concat([shard, replace]),
                            []
                        )
                        .slice(0, -1)
                : input;
        } else if (Array.isArray(input)) {
            return input
                .map(shard =>
                    typeof shard == 'string'
                        ? tools.replaceHTML(shard, search, replace, all)
                        : shard
                )
                .reduce((accum, shard) => accum.concat(shard), []);
        }
        return '';
    },
    pNested: (path, obj) => {
        return String(path)
            .split('.')
            .reduce((obj, next) => (obj && obj[next] ? obj[next] : null), obj);
    }
};

export default tools;
