import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as FontAwesomeSolid from '@fortawesome/free-solid-svg-icons';
import * as FontAwesomeBrands from '@fortawesome/free-brands-svg-icons';

const faSearch = (base, props) => {
    const keys = Object.keys(base);
    const search = Object.keys(props || {})
        .map(name => 'fa' + name.substr(0, 1).toUpperCase() + name.substr(1))
        .filter(slug => keys.indexOf(slug) >= 0)[0];
    return search ? base[search] : null;
};

function FasIcon({ className, ...others }) {
    const icon = faSearch(
        FontAwesomeSolid,
        others.icon || 0 ? { [others.icon]: 1 } : others
    );
    return icon ? <FontAwesomeIcon className={className} icon={icon} /> : null;
}

function FabIcon({ className, ...others }) {
    const icon = faSearch(
        FontAwesomeBrands,
        others.icon || 0 ? { [others.icon]: 1 } : others
    );
    return icon ? <FontAwesomeIcon className={className} icon={icon} /> : null;
}

function FaIcon(props) {
    return FasIcon(props) || FabIcon(props);
}

export { FaIcon, FasIcon, FabIcon };
