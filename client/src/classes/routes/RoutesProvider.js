import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch, Redirect } from 'react-router';
import universal from 'react-universal-component';
import ScrollTop from '../helpers/scrolltop';
import { lang } from 'root/classes';
import { store } from 'store';

const UniversalComponent = universal(props => import(`root/${props.page}`), {
    loading: () => null
});

class RoutesProvider extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        const { routes, notFound } = this.props;
        /* Main render */
        return (
            <Router>
                <ScrollTop>
                    <Switch>
                        {[
                            (routes.items || []).map(route =>
                                this.renderRoute(route)
                            )
                        ]}
                        {notFound && (
                            <Route
                                render={routeProps => (
                                    <UniversalComponent
                                        page={notFound}
                                        {...routeProps}
                                    />
                                )}
                            />
                        )}
                    </Switch>
                </ScrollTop>
            </Router>
        );
    }

    renderRoute(route) {
        const { path, tpl, alt } = route;
        /* Lang checking */
        const checkLang = lang.isEqual(route.lang || lang.get());
        /* A simple route */
        if (checkLang && Boolean(path) && Boolean(tpl)) {
            return (
                <Route
                    exact
                    path={path}
                    render={routeProps => (
                        <UniversalComponent page={tpl} {...routeProps} />
                    )}
                />
            );
        }
        /* Redirect */
        if (!checkLang && Boolean(alt)) {
            let routes = Object.entries(alt)
                .filter(([key]) => lang.isEqual(key))
                .map(([key, route]) => route);
            /* An alternative link is found */
            if (routes.length) {
                return <Redirect exact from={path} to={routes[0]} />;
            }
        }
        /* Not found - ignore */
        return;
    }
}

RoutesProvider.propTypes = {
    routes: PropTypes.shape({
        items: PropTypes.arrayOf(
            PropTypes.shape({
                path: PropTypes.string,
                tpl: PropTypes.string,
                alt: PropTypes.object
            })
        )
    }),
    notFound: PropTypes.any
};

export default store.connect(['lang', 'routes:routesReducer'])(RoutesProvider);
