import * as Reducers from './routes.reducers';
import * as Actions from './routes.actions';
import { store } from 'store';
export { Actions };

store.addReducers(Reducers);
store.addActions(Actions);
