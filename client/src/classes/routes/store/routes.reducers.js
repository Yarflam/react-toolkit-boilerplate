import * as Actions from './routes.actions';
import routes from 'root/assets/json/routes';

/* Default State */
const defaultState = {
    items: routes
};

export function routesReducer(state = defaultState, action) {
    switch (action.type) {
        case Actions.ROUTE_LOAD: {
            return Object.assign({}, state, {
                items: state.items.concat(action.data)
            });
        }

        case Actions.ROUTE_ERROR: {
            return Object.assign({}, state, {
                error: action.error
            });
        }

        case Actions.ROUTE_ADD: {
            state.items.push(action.data);
            return state;
        }

        default: {
            return state;
        }
    }
}
