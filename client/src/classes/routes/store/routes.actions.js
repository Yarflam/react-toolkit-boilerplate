/* Types */

export const ROUTE_LOAD = 'ROUTE_LOAD';
export const ROUTE_ERROR = 'ROUTE_ERROR';
export const ROUTE_ADD = 'ROUTE_ADD';

/* Messages */

export function msgRouteLoad(data) {
    return { type: ROUTE_LOAD, data };
}

export function msgRouteError(error) {
    return { type: ROUTE_ERROR, error };
}

export function msgRouteAdd(lang) {
    return { type: LANG_SELECT, use: lang };
}

/* Calls */

export function getRoutes() {
    return dispatch => {
        dispatch(msgRouteError('Error: getRoutes is not implemented.'));
    };
}

export function addRoute(lang) {
    return dispatch => {
        dispatch(msgRouteAdd(lang));
    };
}
