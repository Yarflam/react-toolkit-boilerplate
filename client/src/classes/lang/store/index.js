import * as Reducers from './lang.reducers';
import * as Actions from './lang.actions';
import { store } from 'store';
export { Actions };

store.addReducers(Reducers);
store.addActions(Actions);
