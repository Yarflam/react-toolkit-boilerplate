/* Types */

export const LANG_LOAD = 'LANG_LOAD';
export const LANG_ERROR = 'LANG_ERROR';
export const LANG_SELECT = 'LANG_SELECT';
export const LANG_DEFAULT_SELECT = 'LANG_DEFAULT_SELECT';

/* Messages */

export function msgLangLoad(data) {
    return { type: LANG_LOAD, data };
}

export function msgLangError(error) {
    return { type: LANG_ERROR, error };
}

export function msgLangSelect(lang) {
    return { type: LANG_SELECT, use: lang };
}

export function msgLangDefaultSelect(lang) {
    return { type: LANG_DEFAULT_SELECT, useDefault: lang };
}

/* Calls */

export function getLang() {
    return dispatch => {
        dispatch(msgLangError('Error: getLang is not implemented.'));
    };
}

export function setLang(lang) {
    return dispatch => {
        dispatch(msgLangSelect(lang));
    };
}

export function setDefaultLang(lang) {
    return dispatch => {
        dispatch(msgLangDefaultSelect(lang));
    };
}
