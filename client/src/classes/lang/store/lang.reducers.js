import * as Actions from './lang.actions';
import * as pack from 'root/assets/json/lang';

/* Default State */
const defaultState = {
    use: 'fr',
    useDefault: 'fr',
    init: false,
    ...pack
};

export function langReducer(state = defaultState, action) {
    switch (action.type) {
        case Actions.LANG_LOAD: {
            Object.entries(action.data).map(([key, values]) => {
                state[key] = { ...(state[key] || {}), ...values };
                return;
            });
            return Object.assign({}, state, {
                init: true
            });
        }

        case Actions.LANG_ERROR: {
            return Object.assign({}, state, {
                init: true,
                error: action.error
            });
        }

        case Actions.LANG_SELECT: {
            return Object.assign({}, state, {
                init: true,
                use: action.use
            });
        }

        case Actions.LANG_DEFAULT_SELECT: {
            return Object.assign({}, state, {
                useDefault: action.useDefault
            });
        }

        default: {
            return state;
        }
    }
}
