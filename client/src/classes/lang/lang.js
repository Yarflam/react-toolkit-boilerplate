import React, { Component } from 'react';
import * as pack from 'root/assets/json/lang';
import { tools } from 'root/classes';
import { store } from 'store';

const lang = {
    /* Get the current language */
    get: () => {
        return store.getState().langReducer.use;
    },

    /* Get the default language */
    getDefault: () => {
        return store.getState().langReducer.useDefault;
    },

    /* Translation + binding */
    print: (name, ...binds) => {
        let raw = lang.printRaw(name);
        /* Replacement */
        binds.map(bind => (raw = tools.replaceHTML(raw, '%s', bind)));
        /* Clean up and return */
        return tools.replaceHTML(raw, '%s', '', true);
    },

    /* Translation */
    printRaw: name => {
        const state = store.getState().langReducer;
        /* Alias (fr-FR -> fr) */
        if (state.use.indexOf('-') >= 0 && !state[state.use]) {
            state.use = state.use.split('-')[0];
        }
        /* Pack or default pack */
        if (!state[state.use]) {
            state.use = state.useDefault;
            /* The developper is a looser */
            if (!state[state.use]) {
                console.warn('[LOOSER] You didn\'t define a default language.');
                return name;
            }
        }
        /* Translation */
        let words = tools.pNested(name, state[state.use]);
        return words ? words : name;
    },

    /* Compare with the current lang */
    isEqual: lang => {
        const state = store.getState().langReducer;
        /* Is equal ! */
        if (state.use == lang) {
            return true;
        }
        /* Alias (fr-FR -> fr) */
        if (state.use.indexOf('-') >= 0 && !state[state.use]) {
            state.use = state.use.split('-')[0];
            if (state.use == lang) {
                return true;
            }
        }
        return false;
    },

    /* Get the language pack */
    getPack: () => {
        return pack;
    }
};

export default lang;
