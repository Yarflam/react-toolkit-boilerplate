import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { store } from 'store';
import lang from './lang';

function LangProvider(props) {
    /* Initialize */
    if (!props._lang.init) {
        let useLang = props.lang;
        let useDefaultLang = props.defaultLang;
        /* Select a language */
        if (useLang) {
            /* Autodetect */
            if (useLang == 'auto' && typeof window !== 'undefined') {
                useLang =
                    window.navigator.language ||
                    window.navigator.userLanguage ||
                    'auto';
            }
            /* Applying */
            if (useLang != 'auto' && useLang != lang.get()) {
                props.setLang(useLang);
            }
        }
        /* Select a default language */
        if (useDefaultLang) {
            if (useDefaultLang != lang.getDefault()) {
                props.setDefaultLang(useDefaultLang);
            }
        }
    }
    /* Show the children */
    return <Fragment>{props.children}</Fragment>;
}

LangProvider.propTypes = {
    _lang: PropTypes.object.isRequired,
    setLang: PropTypes.func.isRequired,
    setDefaultLang: PropTypes.func.isRequired,
    lang: PropTypes.string,
    defaultLang: PropTypes.string,
    children: PropTypes.any
};

export default store.connect(
    ['_lang:langReducer'],
    ['setLang', 'setDefaultLang']
)(LangProvider);
