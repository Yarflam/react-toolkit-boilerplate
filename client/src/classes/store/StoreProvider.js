import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import store from './store';

function StoreProvider(props) {
    return <Provider store={store}>{props.children}</Provider>;
}

StoreProvider.propTypes = {
    children: PropTypes.any.isRequired
};

export default StoreProvider;
