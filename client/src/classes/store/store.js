import {
    createStore,
    applyMiddleware,
    combineReducers,
    bindActionCreators
} from 'redux';
import { connect } from 'react-redux';
import thunkMiddleware from 'redux-thunk';

/* Reducers and Actions */
let listReducers = {};
let listActions = {};

let store = {
    /* Redux Store */
    ...createStore(() => {}, applyMiddleware(thunkMiddleware)),

    /* Add a reducer */
    addReducer: (name, reducer) => {
        listReducers[name] = reducer;
        store.replaceReducer(combineReducers(listReducers));
    },

    addReducers: reducers => {
        Object.entries(reducers).map(reducer => store.addReducer(...reducer));
    },

    /* Add an action */
    addAction: (name, action) => {
        listActions[name] = action;
    },

    addActions: actions => {
        Object.entries(actions).map(action => store.addAction(...action));
    },

    getActions: () => {
        return listActions;
    },

    /* Binding */
    connect: (states, dispatches) => {
        return connect(
            /* States to props */
            currentState =>
                (states || []).reduce((accum, name) => {
                    let [key, item] = name.split(':');
                    accum[key] = Boolean(currentState[item || key])
                        ? currentState[item || key]
                        : {};
                    return accum;
                }, {}),
            /* Dispatch to props */
            dispatch =>
                bindActionCreators(
                    (dispatches || []).reduce((accum, name) => {
                        let [key, item] = name.split(':');
                        accum[key] = Boolean(listActions[item || key])
                            ? listActions[item || key]
                            : {};
                        return accum;
                    }, {}),
                    dispatch
                )
        );
    }
};

export default store;
