const gulp = require('gulp');
const realFavicon = require('gulp-real-favicon');
const path = require('path');

const configPublic = require('./config/public.cfg.json');

/*
 *	Create the icons (oneshot)
 */
gulp.task('favicon', async () => {
    return await realFavicon.generateFavicon({
        masterPicture: path.resolve(...configPublic.favicon.source),
        dest: path.resolve(...configPublic.favicon.dest),
        iconsPath: '/',
        design: {
            ios: {
                pictureAspect: 'backgroundAndMargin',
                backgroundColor: '#ffffff',
                margin: '0%',
                assets: {
                    ios6AndPriorIcons: false,
                    ios7AndLaterIcons: false,
                    precomposedIcons: false,
                    declareOnlyDefaultIcon: true
                },
                appName: configPublic.app.title
            },
            desktopBrowser: {},
            windows: {
                pictureAspect: 'noChange',
                backgroundColor: '#ffffff',
                onConflict: 'override',
                assets: {
                    windows80Ie10Tile: true,
                    windows10Ie11EdgeTiles: {
                        small: true,
                        medium: true,
                        big: true,
                        rectangle: true
                    }
                },
                appName: configPublic.app.title
            },
            androidChrome: {
                pictureAspect: 'noChange',
                themeColor: '#ffffff',
                manifest: {
                    name: configPublic.app.title,
                    startUrl: configPublic.client.host,
                    display: 'browser',
                    orientation: 'notSet',
                    onConflict: 'override',
                    declared: true
                },
                assets: {
                    legacyIcon: false,
                    lowResolutionIcons: false
                }
            },
            safariPinnedTab: {
                pictureAspect: 'blackAndWhite',
                threshold: 95,
                themeColor: '#AAFF00'
            }
        },
        settings: {
            compression: 2,
            scalingAlgorithm: 'Mitchell',
            errorOnImageTooSmall: false,
            readmeFile: false,
            htmlCodeFile: false,
            usePathAsIs: false
        },
        markupFile: path.resolve(...configPublic.favicon.data)
    });
});
