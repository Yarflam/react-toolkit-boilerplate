const lzma = require('lzma');

const bytesToHex = bytes => {
    for (var hex = [], i = 0; i < bytes.length; i++) {
        var current = bytes[i] < 0 ? bytes[i] + 256 : bytes[i];
        hex.push((current >>> 4).toString(16));
        hex.push((current & 0xf).toString(16));
    }
    return hex.join('');
};

/* LzmaPlugin by Yarflam */
module.exports = class Lzma {
    constructor(feed) {
        this.name = 'LzmaPlugin';
        this.feed = feed;
    }

    apply(compiler) {
        compiler.hooks.emit.tapAsync(this.name, (compilation, callback) => {
            if (this.feed) {
                Object.keys(compilation.assets)
                    .filter(name => Boolean(this.feed(name)))
                    .map(name => {
                        var source = compilation.assets[name]._value;
                        /* Applying the changes */
                        compilation.assets[name]._value =
                            'new Function(lzma.decompress(hexToBytes("' +
                            bytesToHex(lzma.compress(source)) +
                            '")))();';
                    });
            }
            callback();
        });
    }
};
