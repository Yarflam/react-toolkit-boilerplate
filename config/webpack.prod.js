const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const LzmaWebpackPlugin = require('./lzma-webpack-plugin');
const path = require('path');

const configPublic = require('./public.cfg.json');
const webpackSets = require('./webpack.sets');

module.exports = [
    {
        mode: 'production',
        entry: [path.resolve(__dirname, 'lzma-extension.js')],
        output: {
            path: webpackSets.public_path,
            publicPath: '/build/',
            filename: 'lzma.js'
        }
    },
    {
        mode: 'production',
        entry: ['@babel/polyfill', './client/src/index.js'],
        output: {
            path: webpackSets.public_path,
            publicPath: '/build/',
            filename: 'app.bundle.js'
        },
        resolve: {
            alias: {
                ...webpackSets.alias
            }
        },
        module: {
            rules: [webpackSets.jsx, webpackSets.scss, ...webpackSets.fonts]
        },
        optimization: {
            minimizer: [
                new UglifyJsPlugin({
                    cache: true,
                    parallel: true,
                    sourceMap: false
                }),
                new OptimizeCssAssetsPlugin({
                    cssProcessorOptions: {
                        discardComments: { removeAll: true }
                    },
                    canPrint: true
                })
            ]
            // splitChunks: {
            //     chunks: 'async',
            //     minSize: 30000,
            //     maxSize: 200000
            // }
        },
        plugins: [
            new LzmaWebpackPlugin(name => name.indexOf('.bundle.js') >= 0),
            new CleanWebpackPlugin(),
            new HtmlWebpackPlugin({
                title: configPublic.app.title,
                hash: true,
                inject: true,
                compile: true,
                favicon: false,
                minify: true,
                cache: true,
                showErrors: true,
                chunks: 'all',
                xhtml: true,
                chunksSortMode: 'none'
            })
        ]
    }
];
