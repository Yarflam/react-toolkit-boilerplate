const webpackSets = require('./webpack.sets');

module.exports = {
    mode: 'development',
    entry: [
        '@babel/polyfill',
        'webpack-hot-middleware/client?reload=true',
        './client/src/index.js'
    ],
    output: {
        path: webpackSets.public_path,
        publicPath: '/build/',
        filename: 'app.bundle.js'
    },
    resolve: {
        alias: {
            ...webpackSets.alias
        }
    },
    module: {
        rules: [webpackSets.jsx, webpackSets.scss, ...webpackSets.fonts]
    },
    ...webpackSets.devmode
};
