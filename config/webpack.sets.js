const webpack = require('webpack');
const path = require('path');

const configPublic = require('./public.cfg.json');

/* Project path */
const pathProject = [__dirname, '..'];
/* Source path */
const pathSource = [...pathProject, ...configPublic.client.source];
/* Public path */
const pathPublic = [...pathProject, ...configPublic.client.expose];

module.exports = {
    public_path: path.resolve(...pathPublic, 'build'),
    alias: {
        'react-dom': '@hot-loader/react-dom',
        root: path.resolve(...pathSource),
        store: path.resolve(...pathSource, ...['classes', 'store'])
    },
    jsx: {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: [
            'cache-loader',
            {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/env']
                }
            }
        ]
    },
    scss: {
        test: /\.(p|s)?css$/,
        use: [
            'cache-loader',
            'style-loader',
            {
                loader: 'css-loader',
                options: {
                    modules: 'global',
                    sourceMap: true,
                    localIdentName: '[name]_[local]_[hash:base64:5]'
                }
            },
            {
                loader: 'postcss-loader',
                options: {
                    sourceMap: true,
                    ident: 'postcss'
                }
            }
        ]
    },
    fonts: [
        {
            test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url-loader?limit=10000&mimetype=application/font-woff'
        },
        {
            test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url-loader?limit=10000&mimetype=application/font-woff'
        },
        {
            test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url-loader?limit=10000&mimetype=application/octet-stream'
        },
        {
            test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'file-loader'
        },
        {
            test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
            loader: 'url-loader?limit=10000&mimetype=image/svg+xml'
        }
    ],
    devmode: {
        devtool: 'source-map',
        devServer: {
            publicPath: '/build/',
            hot: true,
            inline: true
        },
        watch: true,
        watchOptions: {
            ignored: /node_modules/
        },
        stats: {
            colors: true,
            hash: false,
            version: false,
            timings: false,
            assets: false,
            chunks: false,
            modules: false,
            reasons: false,
            children: false,
            source: false,
            errors: true,
            errorDetails: false,
            warnings: false,
            publicPath: false
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new webpack.NoEmitOnErrorsPlugin()
        ]
    }
};
