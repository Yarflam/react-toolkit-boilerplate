var lzma = require('lzma/src/lzma_worker.js').LZMA;

const hexToBytes = hex => {
    for (var bytes = [], c = 0; c < hex.length; c += 2)
        bytes.push(parseInt(hex.substr(c, 2), 16));
    return bytes;
};

if (typeof window !== 'undefined') {
    window.lzma = lzma;
    window.hexToBytes = hexToBytes;
}
