/*
 *	ACCESS
 *	[post/get/put/delete]: [url/regex]
 *		Create -> POST
 *		Read -> GET
 *		Update -> PUT
 *		Delete -> DELETE
 *
 *	SECURITY
 *	secure: [control_name]
 */

export const read = {
    get: '/test',
    secure: props => {
        //
    }
};
