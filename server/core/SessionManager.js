import path from 'path';

class SessionManager {
    constructor({ pathRoot, cookie, folder, ext }) {
        this.cookie = cookie || 'SUID';
        this.folder = path.resolve(pathRoot, ...folder);
        this.ext = ext || 'sid';
    }

    connect(app, source) {
        switch (source) {
            case 'express':
                app.use((req, res) => {
                    // treatment
                    req.next();
                });
                break;
            case 'socket.io':
                app.on('connection', socket => {
                    // treatment
                });
                break;
            default:
                break;
        }
    }
}

export default SessionManager;
