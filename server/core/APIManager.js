class APIManager {
    constructor() {
        this._handles = {};
    }

    connect(app, source) {
        switch (source) {
            case 'express':
                this._handles[source] = app;
                break;
            case 'socket.io':
                this._handles[source] = app;
                break;
            default:
                break;
        }
    }

    load(jobs) {
        //
    }
}

export default APIManager;
