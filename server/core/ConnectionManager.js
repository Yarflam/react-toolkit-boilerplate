import express from 'express';
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import socketIo from 'socket.io';
import path from 'path';
import http from 'http';

import SessionManager from './SessionManager';
import APIManager from './APIManager';

class ConnectionManager {
    constructor({ pathRoot, host, port, session, publish }) {
        this.pathRoot = pathRoot;
        this.host = host || '0.0.0.0';
        this.port = port || 3000;
        this.session = new SessionManager({ pathRoot, ...session });
        this.api = new APIManager();
        this.build({ publish });
    }

    build({ publish }) {
        /* Headers */
        const headers = res => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header(
                'Access-Control-Allow-Headers',
                'Origin, X-Requested-With, Content-Type, Accept'
            );
            res.header('Access-Control-Request-Method', 'POST,GET,PUT,DELETE');
            res.next();
        };

        /* Create Express application */
        this.app = express();
        this.app.use(
            bodyParser.urlencoded({
                extended: true
            })
        );
        this.app.use(bodyParser.json());
        this.app.use(cookieParser());
        this.app.use(headers);
        this.session.connect(this.app, 'express');
        this.api.connect(this.app, 'express');

        /* Add socket.io */
        this.server = http.Server(this.app);
        this.io = socketIo(this.server);
        this.session.connect(this.io, 'socket.io');
        this.api.connect(this.io, 'socket.io');

        /* Add a publish folder */
        if (publish || false) {
            this.app.use(
                express.static(path.resolve(this.pathRoot, ...publish))
            );
        }

        /* Listener */
        this.app.listen(this.port, this.host, () => {
            console.log(`The server is working on :${this.port}.`);
        });
    }
}

export default ConnectionManager;
