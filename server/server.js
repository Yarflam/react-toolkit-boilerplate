import { ConnectionManager, APIManager } from './core';
import config from '../config/public.cfg.json';
import jobs from './api';
import path from 'path';

/* Create a new connection */
const server = new ConnectionManager({
    pathRoot: path.resolve(__dirname, '..'),
    /* Express Server */
    host: config.server.host,
    port: config.server.port,
    publish: config.server.publish,
    /* Session */
    session: config.server.session
});

/* Load the API jobs */
server.api.load(jobs);
